//
//  car.swift
//  curves
//
//  Created by Private on 2/25/18.
//  Copyright © 2018 Private. All rights reserved.
//

import UIKit
import CoreGraphics

class car: UIView {
    
    private var shapeLayer = CAShapeLayer()
    private var shapeLayer2 = CAShapeLayer()
    private var shapeLayer3 = CAShapeLayer()
    private var shapeLayer4 = CAShapeLayer()
    
    override func draw(_ rect:CGRect) {
        drawLine()
    }
    private func drawLine() {

        let aPath = UIBezierPath()
        let bPath = UIBezierPath()
        let cPath = UIBezierPath()
        let dPath = UIBezierPath()
        
        aPath.move(to: CGPoint(x:20, y: 100))
        aPath.addLine(to: CGPoint(x:10, y: 180))
        aPath.addLine(to: CGPoint(x:400, y: 180))
        aPath.addCurve(to: CGPoint( x: 230, y: 110),
                       controlPoint1: CGPoint(x: 400, y: 130),
                       controlPoint2: CGPoint(x: 400, y: 150))
        aPath.addCurve(to: CGPoint( x: 20, y: 100),
                       controlPoint1: CGPoint(x: 100, y: 50),
                       controlPoint2: CGPoint(x: 100, y: 50))
        
        bPath.move(to: CGPoint(x:20, y: 110))
        bPath.addLine(to: CGPoint(x:230, y: 110))
        
//        cPath.move(to: CGPoint(x:50, y: 180))
//        cPath.addCurve(to: CGPoint( x: 100, y: 180),
//                       controlPoint1: CGPoint(x: 400, y: 130),
//                       controlPoint2: CGPoint(x: 400, y: 150))
//        
        shapeLayer.removeFromSuperlayer()
        shapeLayer.fillColor = UIColor.orange.cgColor
        shapeLayer.strokeColor = UIColor.yellow.cgColor
        shapeLayer.lineWidth = 6
        shapeLayer.path = aPath.cgPath
        self.layer.addSublayer(shapeLayer)
        
        shapeLayer2.removeFromSuperlayer()
        shapeLayer2.fillColor = UIColor.orange.cgColor
        shapeLayer2.strokeColor = UIColor.yellow.cgColor
        shapeLayer2.lineWidth = 6
        shapeLayer2.path = bPath.cgPath
        self.layer.addSublayer(shapeLayer2)
        
        shapeLayer2.removeFromSuperlayer()
        shapeLayer2.fillColor = UIColor.black.cgColor
        shapeLayer2.strokeColor = UIColor.black.cgColor
        shapeLayer2.lineWidth = 6
        shapeLayer2.path = cPath.cgPath
        self.layer.addSublayer(shapeLayer3)
    }
}

