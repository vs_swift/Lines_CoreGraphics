//
//  BezView.swift
//
//  Created by Private on 2/25/18.
//  Copyright © 2018 Private. All rights reserved.
//

import UIKit
import CoreGraphics

class BezView: UIView {
    private var shapeLayer = CAShapeLayer()
    private var shapeLayer2 = CAShapeLayer()
    
    override func draw(_ rect:CGRect) {
        drawLine()
    }
    private func drawLine() {

        let aPath = UIBezierPath()
        let bPath = UIBezierPath()

        
        aPath.move(to: CGPoint(x:0, y: 200))
        aPath.addCurve(to: CGPoint( x: 100, y: 80),
                       controlPoint1: CGPoint(x: 0, y: 0),
                       controlPoint2: CGPoint(x: 100, y: 0))
        aPath.move(to: CGPoint(x:0, y: 0))
        
        bPath.move(to: CGPoint(x:100, y: 80))
        bPath.addCurve(to: CGPoint( x: 200, y: 200),
                       controlPoint1: CGPoint(x: 150, y: 0),
                       controlPoint2: CGPoint(x: 180, y: 0))
        bPath.move(to: CGPoint(x:0, y: 0))
        
        shapeLayer.removeFromSuperlayer()
        shapeLayer.fillColor = UIColor.green.cgColor
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.lineWidth = 6
        shapeLayer.path = aPath.cgPath
        self.layer.addSublayer(shapeLayer)
        
        shapeLayer2.removeFromSuperlayer()
        shapeLayer2.fillColor = UIColor.green.cgColor
        shapeLayer2.strokeColor = UIColor.red.cgColor
        shapeLayer2.lineWidth = 6
        shapeLayer2.path = bPath.cgPath
        self.layer.addSublayer(shapeLayer2)
    }
}


